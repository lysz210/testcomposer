<?php
namespace ciao\testcomposer;

class MyClass {
    function __construct() {
        print("My Test construction...");
    }

    function __destruct() {
        print("Destroying My Test!");
    }
}
 ?>
